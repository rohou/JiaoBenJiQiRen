package com.jlh;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * com.jlh
 * Created by ASUS on 2017/9/24.
 * 14:40
 * 脚本内容
 */
public class RobotMain {
    public static void main(String[] args) throws AWTException {
        outPutHello();
    }

    public static void outPutHello(){
        Robot robot = null;
        try {
            robot = new Robot();
            robot.setAutoDelay(500);
            click(robot,KeyEvent.VK_H);
            click(robot,KeyEvent.VK_E);
            click(robot,KeyEvent.VK_L);
            click(robot,KeyEvent.VK_L);
            click(robot,KeyEvent.VK_O);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    private static void click(Robot robot,int keyEvent){
        robot.keyPress(keyEvent);
        robot.keyRelease(keyEvent);
    }
}
